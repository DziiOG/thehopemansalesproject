import React, { Component } from 'react'


const ProductContext = React.createContext();

 class ProductProvider extends Component {

    state={
        searchBarFocused: false,
        messageSearchBarFocused: false,
        notificationButton1Focused: true,
        notificationButton2Focused: false,
        inputAutoFocus: false,
        id: {}
    }

    keyboardDidShow = () => {

        this.setState({searchBarFocused: true})
    }

    keyboardWillHide = () => {
        this.setState({searchBarFocused: false})
    }

    keyboardDidShow1 = () => {

        this.setState({
        messageSearchBarFocused: true,
        inputAutoFocus: true
        })
    }

    keyboardWillHide1 = () => {
        this.setState({
            messageSearchBarFocused: false,
            inputAutoFocus: false
        })
    }




    notificationButtonSwitcher = () => {
        if(this.state.notificationButton1Focused == true){
            this.setState({
                notificationButton1Focused: false,
                notificationButton2Focused: true
            })
        }   
   
    }

    notificationButtonSwitcher1 = () => {
        if(this.state.notificationButton2Focused == true){
            this.setState({
                notificationButton1Focused: true,
                notificationButton2Focused: false
            })
        }   
   
    }
    getId = (id) => {
        this.setState({
            id: id
        }) 
    }


    render() {
        return (
           <ProductContext.Provider
           value={{...this.state,
           keyboardDidShow: this.keyboardDidShow,
           keyboardWillHide: this.keyboardWillHide,
           keyboardWillShow: this.keyboardDidShow,
           notificationButtonSwitcher: this.notificationButtonSwitcher,
           notificationButtonSwitcher1: this.notificationButtonSwitcher1,
           keyboardDidShow1: this.keyboardDidShow1,
           keyboardWillHide1: this.keyboardWillHide1,
           keyboardWillShow1: this.keyboardDidShow1,
           getId: this.getId
           
           }}
           >
               {this.props.children}
           </ProductContext.Provider>
        )
    }
}

const ProductConsumer = ProductContext.Consumer;
export {ProductProvider, ProductConsumer}
