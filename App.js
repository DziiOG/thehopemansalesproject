import 'react-native-gesture-handler';
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import Root from './src/main';









export default function App() {
  return (
    <>
    <View style={styles.container}>
        <Root {...this.props} />
    </View>
    </>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});