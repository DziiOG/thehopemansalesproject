import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Login from '../components/Login';
import {
 
} from '../modules/login';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Login);
