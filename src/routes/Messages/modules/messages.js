import update from "react-addons-update";
import constants from './actionConstants';

const {
    GET_USER_ID,
    GET_MESSAGE_ITEM
} = constants;

//Actions
export function getUserID(id){
    return({
        type: GET_USER_ID,
        payload: id
        }
    )
}

export function getMessageItem(item){
    return (
        (dispatch => {
            dispatch({
                type: GET_MESSAGE_ITEM,
                payload: item
            })
        })
    )
    
}
















//Action Handlers
function handleGetUserID(state, action){
    
    return update(state, {
        userID: {
            $set: action.payload
        }
    })
}

function handleGetMessageItem(state, action){

    return update(state,
        {
            messageItem: {
                $set: action.payload
            }
        }
        )
}






const ACTION_HANDLERS = {
    GET_USER_ID: handleGetUserID,
    GET_MESSAGE_ITEM: handleGetMessageItem
}

const initialState = {
    userID: {},
    messageItem: {}
};

export function messagesReducer (state = initialState, action){
    const handler = ACTION_HANDLERS[action.type];

    return handler ? handler(state, action) : state;

}