import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Messages from '../components/Messages';
import {
 getUserID,
 getMessageItem
} from '../modules/messages';

const mapStateToProps = state => ({
 userID: state.messages.userID || null,
 messageItem: state.messages.messageItem || {}
});

const mapActionsCreators = {
  getUserID,
  getMessageItem
};

export default connect(mapStateToProps, mapActionsCreators)(Messages);
