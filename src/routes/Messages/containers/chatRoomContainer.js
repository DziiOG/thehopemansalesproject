import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import chatRoom from '../components/chatRoom';
import {
 getMessageItem
} from '../modules/messages';

const mapStateToProps = state => ({
    messageItem: state.messages.messageItem || {}
});

const mapActionsCreators = {
  getMessageItem
};

export default connect(mapStateToProps, mapActionsCreators)(chatRoom);
