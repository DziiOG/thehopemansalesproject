import React, { Component, Fragment } from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import HeaderComponent from '../../Messages/components/Header'
import { ProductConsumer } from '../../../../context'
import SearchBox from './SearchBox'
import {Card, CardItem, Thumbnail, Body, Left, Right, Button, Icon} from 'native-base'
export default class Messages extends Component {


    componentDidMount(){
        //console.log(this.props)
    }
    render() {
        

      const MessagesOP = [
            {
                image: require('../../../assets/me.png'),
                sender: "SpongeBob",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 1
            }, {
                image: require('../../../assets/me.png'),
                sender: "Rosana",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 2
            }, {
                image: require('../../../assets/me.png'),
                sender: "Bob",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 10
            }, {
                image: require('../../../assets/me.png'),
                sender: "SeBob",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 3
            },
            {
                image: require('../../../assets/me.png'),
                sender: "Spob",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 4
            },
            {
                image: require('../../../assets/me.png'),
                sender: "Spobdsrrr",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 5
            },
            {
                image: require('../../../assets/me.png'),
                sender: "lkidos",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 6
            },
            {
                image: require('../../../assets/me.png'),
                sender: "sddSpongeBob",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 7
            },
            {
                image: require('../../../assets/me.png'),
                sender: "rererSpongeBob",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 8
            },
            {
                image: require('../../../assets/me.png'),
                sender: "Spob78",
                latestMessage: "C'mon fuck everybody. Where is patrick?",
                allMessages: [],
                date: "May 2, 2020",
                senderId: 9
            },
        ]



        return (
            <ProductConsumer>
                {
                    (value) => (
                        <View style={{backgroundColor: 'white'}}>
                        {
                            (value.messageSearchBarFocused) ? 
                            (<View>
                                <SearchBox value={value}></SearchBox>
                            </View>) : (
                                <Fragment>
                                    <HeaderComponent {...this.props}></HeaderComponent>
                                    <View>
                                        <SearchBox value={value}></SearchBox>
                                    </View>
                                </Fragment>
                            ) 
                        }
                       

                            <FlatList
                            style={{backgroundColor: 'white'}} 
                            data={MessagesOP} 
                            renderItem={({item})=> <Card>
                                <TouchableOpacity onPress={()=>{
                                    //this.props.getUserID(item.senderId);
                                    //console.log(this.props.userID)
                                    //value.getId(index)
                                    //console.log(value.id)
                                    if(item){

                                    this.props.getMessageItem(item)
                                    this.props.navigation.navigate('chatroom')
                                    }
                                }}>
                                    <CardItem>
                                    <Left>
                                        <TouchableOpacity onPress={()=>{alert("You pressed my ass")}}>
                                            <Thumbnail source={item.image}>
                                            </Thumbnail>
                                        <View>
                                            <Text style={{ fontWeight:'bold'}} note>{item.sender}</Text>
                                        </View>
                                        </TouchableOpacity>
                                        <View style={{ alignItems: 'flex-start', }}>
                                            <Text style={{ paddingHorizontal: 20}}>{item.latestMessage}</Text>   
                                        </View>
                                    </Left>
                                        
                                    <Right style={{bottom: 16, }}>
                                        <Body>
                                            <Text style={{fontWeight:'bold'}} note>{item.date}</Text> 
                                        </Body>
                                        
                                    </Right>
                                    </CardItem>
                                </TouchableOpacity>
                                </Card> }
                            keyExtractor={(item, index)=>  index.toString()  }    
                                />
                        
                        
                        </View>
                    )

                    
                }
            </ProductConsumer>
        )
    }
}
