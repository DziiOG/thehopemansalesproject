import React, { Component } from 'react'
import Icon from "react-native-vector-icons/EvilIcons";
import  {View, InputGroup, Input, Container, Content, Right, Button } from 'native-base';
import * as Animatable from 'react-native-animatable';

import {Keyboard, TouchableOpacity, Text} from 'react-native'
import { ProductConsumer } from '../../../../../context';

export default class SearchBox extends Component {

    

    componentDidMount(){
        this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        this.keyboardWillHide = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
        this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    }


    keyboardDidShow = ()=> {
        //alert('keyboard is showing')
        this.props.value.keyboardDidShow1()
    }

    keyboardWillHide = () => {
        //alert('keyboard is hiding')
        this.props.value.keyboardWillHide1()
    }

    keyboardWillShow = () => {
        //alert('keyboard is showing')
        this.props.value.keyboardWillShow1()
    }


    render() {

        return (
            <ProductConsumer>
                {
                    (value) => (

                <Animatable.View animation="slideInRight" duration={500} style={{
                    backgroundColor: "white",  
                    margin: 5, 
                    marginLeft: 30, 
                    marginRight: 50, 
                    height: 40, 
                    alignItems: 'center', 
                    flexDirection: 'row'}} >

                <InputGroup style={{
                    backgroundColor: 'rgba(221,160,221,0.5)',
                    borderBottomEndRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderTopEndRadius: 20,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    height: 35,
                    width:value.messageSearchBarFocused ? 250 :300
                    }}>
                    <Icon name="search" color="#fff" size={30}></Icon>
                    <Input placeholder="Search" placeholderTextColor="#fff" style={{color: '#dda0dd', fontSize: 24}} autoFocus={value.inputAutoFocus ? true : false}></Input>
                </InputGroup>
                {
                    (value.messageSearchBarFocused) &&
                                                    
                                                        <Right style={{left: 40}}>
                                                                            <Button transparent style={{width: 60}} onPress={()=>{value.keyboardWillHide1(); Keyboard.dismiss()}}>
                                                                                <Text style={{color: "#dda0dd", fontSize: 15}} onPress={()=>{value.keyboardWillHide1(); Keyboard.dismiss()}}>Cancel</Text>
                                                                            </Button>
                                                        </Right>
                }
            </Animatable.View>
                    )
                }
            </ProductConsumer>
        )
    }
}
