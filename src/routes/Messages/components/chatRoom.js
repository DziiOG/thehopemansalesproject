import React, { Component,  } from 'react'
import { Dimensions } from 'react-native'
import { View, Text, Header, Left, Card, CardItem, Body, Thumbnail } from 'native-base'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { GiftedChat } from 'react-native-gifted-chat'


const { width, height } = Dimensions.get('window');

export default class chatRoom extends Component {




    state = {
        messages: [],
    }


    componentDidMount(){
        this.setState({
            messages: [
                {
                    _id: 1,
                    text: 'Hello Spongebob',
                    createdAt: new Date(),
                    
                    user: {
                        _id: 2,
                        name: 'React Native',
                        avatar: this.props.messageItem.image,
                    },
                },
            ],
        })
    }

    onSend( messages = []){
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }


    render() {
        //console.log(this.props)
        const {image,
            sender,
            latestMessage,
            allMessages,
            date,
            senderId } = this.props.messageItem
        return (

           
            
                <GiftedChat 
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                user={{
                    _id:1,
                }}
                showUserAvatar
                ></GiftedChat>
            

            
        )
    }
}
