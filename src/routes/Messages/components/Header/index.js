import React, { Component, Fragment } from 'react';
import { Text, TouchableOpacity, Keyboard, View} from 'react-native';

import { Avatar } from 'react-native-paper';
import { Header, Left, Body, Right, Button, Title} from "native-base";
import { ProductConsumer } from '../../../../../context';


export default class HeaderComponent extends Component {
    render() {
        return (
            <Header style={{backgroundColor: 'white'}}>
            
            <ProductConsumer>
                {
                    (value) => (
                    
                        <Fragment>
                        

                            <Left style={{right: 10}}>
                                <TouchableOpacity onPress={()=> this.props.navigation.openDrawer()}>
                                <Avatar.Image source={{
                                        uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                    }}
                                    size={30}
                                    style={{marginLeft: 15}}
                                    
                                    >

                                    </Avatar.Image>
                                </TouchableOpacity>
                            </Left>
                            <Body style={{marginRight: 75,alignItems: 'center'}}>
                                <Text style={{fontSize: 24, color: '#dda0dd'}}>
                                    Messages
                                </Text>
                            </Body>
                           
                        </Fragment>
                    
                            
                        
                    )
                }
            </ProductConsumer>
            </Header>
        )
    }
}
