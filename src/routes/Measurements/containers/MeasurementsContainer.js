import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Measurements from '../components/Measurements';
import {
 
} from '../modules/measurements';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Measurements);
