import React, { Component, Fragment } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Container, Content, Header, Left, Body, Right, Button, Icon, Title, Segment, Card, CardItem} from 'native-base';
import { ProductConsumer } from '../../../../context';
import CardComponent from './CardComponent';

export default class Notifications extends Component {
    render() {
        return (
            <ProductConsumer>
                {
                    (value)=> (

                    <Container style={{backgroundColor: 'white'}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingLeft: 25, marginTop: 10}}>
                                <TouchableOpacity style={{
                                    right: 15, 
                                    width: 73, 
                                    height:37, 
                                    backgroundColor:value.notificationButton1Focused ?  '#fff' : '#dda0dd' ,
                                        borderBottomEndRadius: 20,
                                        borderBottomLeftRadius: 20,
                                        borderBottomRightRadius: 20,
                                        borderTopEndRadius: 20,
                                        borderTopLeftRadius: 20,
                                        borderTopRightRadius: 20,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    
                                    }}
                                    onPress={()=>{value.notificationButtonSwitcher1()}}
                                    >
                                    <Text style={{color:value.notificationButton1Focused ?  '#dda0dd' : '#fff'}}>Updates</Text>
                                </TouchableOpacity>
                                
                                <TouchableOpacity style={{
                                    width: 73, 
                                    height: 37, 
                                    backgroundColor:value.notificationButton2Focused ?  '#fff' : '#dda0dd',
                                    borderBottomEndRadius: 20,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    borderTopEndRadius: 20,
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                    }}
                                    onPress={()=>{value.notificationButtonSwitcher()}}
                                    >
                                    <Text style={{color:value.notificationButton2Focused ?  '#dda0dd' : '#fff'}}>Mentions</Text>
                                </TouchableOpacity>
                            </View>
                        <Content>
                            <View>
                                    {
                                        (value.notificationButton1Focused) &&(
                                            <Fragment>
                                                <CardComponent></CardComponent>
                                                <CardComponent></CardComponent>
                                                <CardComponent></CardComponent>
                                            </Fragment>
                                        ) 
                                        
                                    }
                                    {
                                        (value.notificationButton2Focused) && 
                                        <View>
                                            <Card>
                                                <CardItem>
                                                    <Body>
                                                        <Text>
                                                        <Text style={{fontWeight: "bold"}}>@spongebob </Text>
                                                            mentioned you in a stupid post
                                                        </Text>
                                                    </Body>
                                                </CardItem>
                                            </Card>
                                        </View>
                                    }

                            </View>
                        </Content>
                    </Container>
                    )
                }
            </ProductConsumer>
        )
    }
}
