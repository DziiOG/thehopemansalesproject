import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import LocationMap from '../components/LocationMap';
import {
 
} from '../modules/locationmap';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(LocationMap);
