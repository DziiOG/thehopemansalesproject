import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Container } from 'native-base'
import MapContainer from './MapContainer'

export default class LocationMap extends Component {
    render() {
        return (
           <Container style={{backgroundColor: 'white'}}>
                <MapContainer></MapContainer>
           </Container>
        )
    }
}
