import React, { Component } from 'react'
import { View } from 'react-native'
import styles from './MapContainerStyles';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';




export default class MapContainer extends Component {

    state={
        region: {}
    }


    componentDidMount(){

    }



    render() {

        mapStyle = [
            {
              elementType: 'geometry',
              stylers: [
                {
                  color: '#e7e9ec',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              elementType: 'labels',
              stylers: [
                {
                  color: '#979797',
                },
              ],
            },
            {
              elementType: 'labels.icon',
              stylers: [
                {
                  color: '#979797',
                },
                {
                  visibility: 'simplified',
                },
              ],
            },
            {
              elementType: 'labels.text',
              stylers: [
                {
                  color: '#677a9f',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#677a9f',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              elementType: 'labels.text.stroke',
              stylers: [
                {
                  color: '#ffffff',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'administrative',
              elementType: 'geometry.fill',
              stylers: [
                {
                  color: '#e7e9ec',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'administrative.land_parcel',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#bdbdbd',
                },
              ],
            },
            {
              featureType: 'landscape',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#e7e9ec',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'landscape.natural.terrain',
              stylers: [
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'landscape.natural.terrain',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#a7dfb6',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'poi',
              elementType: 'labels.text',
              stylers: [
                {
                  color: '#677a9f',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#677a9f',
                },
                {
                  visibility: 'on',
                },
                {
                  weight: 3.5,
                },
              ],
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.stroke',
              stylers: [
                {
                  color: '#ffffff',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#9e9e9e',
                },
              ],
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#ffffff',
                },
              ],
            },
            {
              featureType: 'road.arterial',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#757575',
                },
              ],
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#a7b5db',
                },
              ],
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.icon',
              stylers: [
                {
                  visibility: 'off',
                },
              ],
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#616161',
                },
              ],
            },
            {
              featureType: 'road.local',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#9e9e9e',
                },
              ],
            },
            {
              featureType: 'transit.line',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#e5e5e5',
                },
              ],
            },
            {
              featureType: 'transit.station',
              stylers: [
                {
                  visibility: 'off',
                },
              ],
            },
            {
              featureType: 'transit.station',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#eeeeee',
                },
                {
                  visibility: 'off',
                },
              ],
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.icon',
              stylers: [
                {
                  visibility: 'off',
                },
              ],
            },
            {
              featureType: 'water',
              stylers: [
                {
                  color: '#76d6ff',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [
                {
                  color: '#76d6ff',
                },
              ],
            },
            {
              featureType: 'water',
              elementType: 'geometry.fill',
              stylers: [
                {
                  color: '#abd5f4',
                },
                {
                  visibility: 'on',
                },
              ],
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [
                {
                  color: '#9e9e9e',
                },
              ],
            },
          ];
        



        return (
           <View style={styles.container}>
                <MapView
                showsMyLocationButton={true}
                style={styles.map}
                customMapStyle={mapStyle}
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                    latitude: 6.667,
                    longitude: -1.433,
                    latitudeDelta: 0.1403,
                    longitudeDelta: 0.1903
                }}
                >
                    <Marker
                    coordinate={{
                        latitude: 6.667,
                        longitude: -1.433
                    }}
                    color="#dda0dd"
                    >

                    </Marker>
                </MapView>

           </View>
        )
    }
}
