import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Settings from '../components/Settings';
import {
 
} from '../modules/settings';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Settings);
