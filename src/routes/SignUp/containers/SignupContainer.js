import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Signup from '../components/Signup';
import {
 
} from '../modules/signup';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Signup);
