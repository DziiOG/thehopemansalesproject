import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Search from '../components/Search';
import {
 
} from '../modules/search';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Search);
