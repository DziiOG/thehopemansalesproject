import React, { Component } from 'react'
import Icon from "react-native-vector-icons/EvilIcons";
import  {View, InputGroup, Input, Container, Content, Right } from 'native-base';
import * as Animatable from 'react-native-animatable';

import {Keyboard} from 'react-native'
import { ProductConsumer } from '../../../../../context';

export default class SearchBox extends Component {

    

    componentDidMount(){
      this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
      this.keyboardWillHide = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
      this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    }


    keyboardDidShow = ()=> {
        //alert('keyboard is showing')
        this.props.value.keyboardDidShow()
    }

    keyboardWillHide = () => {
        this.props.value.keyboardWillHide()
    }

    keyboardWillShow = () => {
        this.props.value.keyboardWillShow()
    }


    render() {

        return (
            <ProductConsumer>
                {
                    (value) => (

                <Animatable.View animation="slideInRight" duration={500} style={{
                    backgroundColor: "white",  
                    margin: 5, 
                    marginLeft:value.searchBarFocused ? 0 : 50, 
                    marginRight: 50, 
                    height: 50, 
                    alignItems: 'center', 
                    flexDirection: 'row'}} >

                <InputGroup style={{
                    backgroundColor: 'rgba(221,160,221,0.5)',
                    borderBottomEndRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderTopEndRadius: 20,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    height: 40,
                    width: 250
                    }}>
                    <Icon name="search" color="#fff" size={30}></Icon>
                    <Input placeholder="Search" placeholderTextColor="#fff" style={{color: '#dda0dd', fontSize: 24}}></Input>
                </InputGroup>
            </Animatable.View>
                    )
                }
            </ProductConsumer>
        )
    }
}
