import React, { Component, Fragment } from 'react'
import { Text, TouchableOpacity, Keyboard} from 'react-native';

import { Avatar } from 'react-native-paper';
import { Header, Left, Body, Right, Button} from "native-base";
import SearchBox from '../SearchBox';
import { ProductConsumer } from '../../../../../context';


export default class HeaderComponent extends Component {
    render() {
        return (
            <Header style={{backgroundColor: 'white'}}>
            
            <ProductConsumer>
                {
                    (value) => (
                        <Fragment>
                        {
                            (value.searchBarFocused == false) &&
                                <Left style={{margin: 15}}>
                                    <TouchableOpacity onPress={()=> this.props.navigation.openDrawer()}>
                                    <Avatar.Image source={{
                                            uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                        }}
                                        size={30}
                                        style={{marginLeft: 15}}
                                        
                                        >

                                        </Avatar.Image>
                                    </TouchableOpacity>
                                </Left>
                        }
                            <SearchBox value={value}></SearchBox>
                            {
                                (value.searchBarFocused) && 
                                <Right style={{right: 1}}>
                                    <TouchableOpacity style={{width: 60}} onPress={()=>{value.keyboardWillHide(); Keyboard.dismiss()}}>
                                        <Text style={{color: "#dda0dd", fontSize: 15}}>Cancel</Text>
                                    </TouchableOpacity>
                                </Right>
                            }
                        </Fragment>
                    )
                }
            </ProductConsumer>
            </Header>
        )
    }
}
