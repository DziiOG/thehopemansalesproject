import React, { Component, Fragment } from 'react'
import {FlatList, Keyboard} from 'react-native'
import  {View, Container, Content, Text } from 'native-base';
import SearchBox from './SearchBox';
import HeaderComponent from './Header';
import { ProductConsumer } from '../../../../context';


export default class Search extends Component {





    render() {
        const ListItems = [
            
        ]
        return (
            <View style={{backgroundColor: 'white'}}>
            <HeaderComponent {...this.props}></HeaderComponent>
                <View>
                    <ProductConsumer>
                        {
                            (value) => (
                                <Fragment>
                                {

                                (value.searchBarFocused == false) && (
                                    ListItems.length == 0 ? 
                                    <View style= {{alignItems: 'center'}}>
                                        <Text style={{color: '#dda0dd'}}>No recent Results </Text>
                                    </View>
                                    :
                                    <FlatList
                                    style={{backgroundColor: value.searchBarFocused ? 'rgba(0,0,0,0.3)': 'white'}} 
                                    data={ListItems} 
                                    renderItem={({item})=> <Text style={{padding: 20, fontSize: 20}}>{item}</Text>}
                                    keyExtractor={(item, index)=> index.toString()}    
                                    />
                                    
                                )
                                }

                                 {
                                     (value.searchBarFocused) && 
                                     <View style={{alignItems: 'center'}}>
                                         <Text style={{color: '#dda0dd'}}> Search for Designers, Tailors and People here</Text>
                                     </View>
                                 }   
                                </Fragment>
                            )
                        }
                    </ProductConsumer>
                 
                   
                </View>
            </View>
        )
    }
}
