import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Designs from '../components/Designs';
import {
 
} from '../modules/designs';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Designs);
