import React from 'react';


import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';


import {View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
//import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch
} from 'react-native-paper';


import { NavigationContainer } from '@react-navigation/native';
import MainTabScreen from './tabNavigator';
import OrdersContainer from './Orders/containers/OrdersContainer';
import MeasurementsContainer from './Measurements/containers/MeasurementsContainer';
import SettingsContainer from './Settings/containers/SettingsContainer';
import { createStackNavigator } from '@react-navigation/stack';
import DesignsContainer from './Designs/containers/DesignsContainer';
import SearchContainer from './Search/containers/SearchContainer';
import MessagesContainer from './Messages/containers/MessagesContainer';
import chatRoomContainer from './Messages/containers/chatRoomContainer';



const MeasurementsStack = createStackNavigator();
const SettingsStack = createStackNavigator();
const OrdersStack = createStackNavigator();
const DesignsStack = createStackNavigator();




const Drawers = createDrawerNavigator();
//const Tab = createBottomTabNavigator();

















export default function MyDrawer() {
  return (
    <NavigationContainer >
      <Drawers.Navigator  initialRouteName="Home2" drawerContent={(props)=> <CustomDrawerContent {...props}></CustomDrawerContent>}>
        <Drawers.Screen name="Home2" component={MainTabScreen} />
        <Drawers.Screen name="Orders2" component={OrdersStackScreen} />
        <Drawers.Screen name="Measurements2" component={MeasurementsStackScreen} />
        <Drawers.Screen name="Settings2" component={SettingsStackScreen} />
        <Drawers.Screen name="Designs2" component={DesignsStackScreen} />
        <Drawers.Screen name="Search3" component={SearchContainer} />
        <Drawers.Screen name="Messages" component={MessagesContainer}/>
        <Drawers.Screen name="chatroom" component={chatRoomContainer}/>
      </Drawers.Navigator>
    </NavigationContainer>
  );
}




function CustomDrawerContent(props) {
  const [isDarkTheme, setIsDarkTheme] = React.useState(false);  
      const toggleTheme = () => {
    setIsDarkTheme(!isDarkTheme);
}

  return (
    <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection: 'row', marginTop: 15}}>
                            <Avatar.Image source={{
                                uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                            }}
                            size={50}
                            >

                            </Avatar.Image>
                            <View style={{marginLeft: 15, flexDirection: 'column'}}>
                                <Title style ={styles.title}>Full Name</Title>
                                <Caption>@Username </Caption>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={styles.paragraph, styles.caption}>0</Paragraph>
                                <Caption>  Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={styles.paragraph, styles.caption}>0</Paragraph>
                                <Caption>  Followers</Caption>
                            </View>  
                        </View>
                    </View> 
                    <Drawer.Section style={styles.drawerContent}>
                        <DrawerItem icon={({size}) => (
                                <Icon
                                    name="home-outline"
                                    color="#DDA0DD"
                                    size={size}
                                    >

                                </Icon>
                                
                                )} 
                                label="Home"
                                
                                onPress={()=> {props.navigation.navigate('Home')}}
                                >
                        </DrawerItem>
                        <DrawerItem icon={({color, size}) => (
                                <Icon
                                    name="account-outline"
                                    color="#DDA0DD"
                                    size={size}
                                    >

                                </Icon>
                                )} 
                                label="Measurements"
                                onPress={()=> {props.navigation.navigate('Measurements2')}}
                                >
                        </DrawerItem>
                        <DrawerItem icon={({color, size}) => (
                                <Icon
                                    name="bookmark-outline"
                                    color="#DDA0DD"
                                    size={size}
                                    >

                                </Icon>
                                )} 
                                label="Orders"
                                onPress={()=> {props.navigation.navigate('Orders2')}}
                                >
                        </DrawerItem>
                        <DrawerItem icon={({color, size}) => (
                                <Icon
                                    name="settings-outline"
                                    color="#DDA0DD"
                                    size={size}
                                    >

                                </Icon>
                                )} 
                                label="Designs"
                                onPress={()=> {props.navigation.navigate('Designs2')}}
                                >
                        </DrawerItem>
                        <DrawerItem icon={({color, size}) => (
                                <Icon
                                    name="settings-outline"
                                    color="#DDA0DD"
                                    size={size}
                                    >

                                </Icon>
                                )} 
                                label="Settings"
                                onPress={()=> {props.navigation.navigate('Settings2')}}
                                >
                        </DrawerItem>
                    </Drawer.Section>
                    <Drawer.Section title="Preferences">
                            <TouchableRipple onPress={() => {toggleTheme()}}>
                                <View style={styles.preference}>
                                    <Text>Dark Theme</Text>
                                    <View pointerEvents="none">
                                        <Switch value={isDarkTheme}></Switch>
                                    </View>
                                </View>
                            </TouchableRipple>
                    </Drawer.Section>

                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem icon={({color, size}) => (
                    <Icon
                    name="exit-to-app"
                    color="#DDA0DD"
                    size={size}
                    >

                    </Icon>
                )} label="Sign Out"
                onPress={()=> {console.log('You pressed me')}}
                >

                </DrawerItem>
            </Drawer.Section>
        </View>
  );
}

/*

function MyTabs() {
    return (
      <Tab.Navigator
        initialRouteName="Feed"
        activeColor="#e91e63"
        style={{ backgroundColor: 'tomato' }}
      >
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="home" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Measurements"
          component={Measurements}
          options={{
            tabBarLabel: 'Updates',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="bell" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Orders"
          component={Orders}
          options={{
            tabBarLabel: 'Profile',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="account" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    )
  };

*/

const DesignsStackScreen = ({navigation}) => 
    (
  
    <DesignsStack.Navigator screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props)=> <MaterialCommunityIcons style={{paddingLeft: 10}} onPress={()=> navigation.goBack()} size={30} color="#DDA0DD" name="arrow-left"></MaterialCommunityIcons>)
  }} >
      <DesignsStack.Screen name="Designs" component={DesignsContainer} />
      
  </DesignsStack.Navigator>
    );








const SettingsStackScreen = ({navigation}) => 
    (
  
    <SettingsStack.Navigator screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props)=> <MaterialCommunityIcons style={{paddingLeft: 10}} onPress={()=> navigation.goBack()} size={30} color="#DDA0DD" name="arrow-left"></MaterialCommunityIcons>)
  }} >
      <SettingsStack.Screen name="Settings" component={SettingsContainer} />
      
  </SettingsStack.Navigator>
    );


    const OrdersStackScreen = ({navigation}) => 
    (
  
    <OrdersStack.Navigator screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props)=> <MaterialCommunityIcons style={{paddingLeft: 10}} onPress={()=> navigation.goBack()} size={30} color="#DDA0DD" name="arrow-left"></MaterialCommunityIcons>)
  }} >
      <OrdersStack.Screen name="Measurement" component={OrdersContainer} />
      
  </OrdersStack.Navigator>
    );

    const MeasurementsStackScreen = ({navigation}) => (
  
      <MeasurementsStack.Navigator screenOptions={{
        headerStyle: {
                backgroundColor: "#fff",
                
            },
            headerTintColor: '#DDA0DD',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerTitleAlign: 'center',
            headerLeft: ((props)=> <MaterialCommunityIcons style={{paddingLeft: 10}} onPress={()=> navigation.goBack()} size={30} color="#DDA0DD" name="arrow-left"></MaterialCommunityIcons>)
    }} >
        <MeasurementsStack.Screen name="Measurement" component={MeasurementsContainer} />
        
    </MeasurementsStack.Navigator>
    );



const styles = StyleSheet.create({
  drawerContent: {
      flex: 1,
  },
  userInfoSection: {
      paddingLeft: 20,
  },
  title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
  },
  caption: {
      fontSize: 14,
      lineHeight: 14,
  },
  row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
  },
  section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
  },
  paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
  },
  drawerSection: {
      marginTop: 15,
  },
  bottomDrawerSection: {
      marginBottom: 15,
      borderTopColor: '#f4f4f4',
      borderTopWidth: 1
  },
  preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 15,
  }
})