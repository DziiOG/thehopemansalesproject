
import React from 'react'
import HomeContainer from './Home/containers/HomeContainer';
//import Measurements from './Measurements/containers/Measurements';
//import Orders from './Orders/containers/Orders';
//import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchContainer from './Search/containers/SearchContainer';
import NotificationsContainer from './Notifications/containers/NotificationsContainer';
import LocationMapContainer from './LocationMap/containers/LocationMapContainer';
import ProfileContainer from './Profile/containers/ProfileContainer';
import { Avatar } from 'react-native-paper';
import {Icon} from 'native-base'







//import Settings from './Settings/containers/Settings';
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity } from 'react-native';
//import SearchBox from './Search/containers/SearchBox';
//import MessagesContainer from './Messages/containers/MessagesContainer';





const Tab = createMaterialBottomTabNavigator();

const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const NotificationsStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const LocationMapStack = createStackNavigator();


















const MainTabScreen = ({navigation}) =>     (
    
        
            <Tab.Navigator
                initialRouteName="Home1"
                activeColor="#808080"
                inactiveColor="#DDA0DD"
                
            >
                <Tab.Screen
                name="Home1"
                component={HomeStackScreen}
                options={{
                    tabBarLabel: 'Home',
                    tabBarColor: '#fff',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="home" activeColor="#808080" color="#DDA0DD" size={26} />
                    ),
                }}
                />
                <Tab.Screen
                name="Search1"
                component={SearchStackScreen}
                options={{
                    tabBarLabel: 'Search',
                    tabBarColor: '#fff',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="search-web" activeColor="#808080" color="#DDA0DD" size={26} />
                    ),
                }}
                />
                <Tab.Screen
                name="Notification1"
                component={NotificationsStackScreen}
                options={{
                    tabBarLabel: 'Notifications',
                    tabBarColor: '#fff',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="bell" activeColor="#808080" color="#DDA0DD" size={26} />
                    ),
                }}
                 
                />
                <Tab.Screen
                name="LocationMap1"
                component={LocationMapStackScreen}
                options={{
                    tabBarLabel: 'Location',
                    tabBarColor: '#fff',
                    tabBarIcon: ({ color }) => (
                    <Icon name="navigate" style={{color: "#DDA0DD"}} size={26} />
                    ),
                }}></Tab.Screen>
                 <Tab.Screen
                name="Profile1"
                component={ProfileStackScreen}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarColor: '#fff',
                    tabBarIcon: ({ color }) => (
                    <MaterialCommunityIcons name="account" activeColor="#808080" color="#DDA0DD" size={26} />
                    ),
                }}></Tab.Screen>
            </Tab.Navigator>
        
    );



    
const HomeStackScreen = ({navigation}) => (

    <HomeStack.Navigator screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props) => 
                        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
                            <Avatar.Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                }}
                                size={30}
                                style={{marginLeft: 15}}
                                
                                >

                                </Avatar.Image>
                        </TouchableOpacity>),
          headerRight: ((props) => <Icon name="ios-send" onPress={()=>{navigation.navigate('Messages')}} size={30} style={{paddingRight: 15, color: "#DDA0DD"}}></Icon>)
  }} >
      <HomeStack.Screen name="Home"  component={HomeContainer} options={{
         
          
      }} />
      
      
  </HomeStack.Navigator>
  );
  
  
  
  
  
  
  
  
  
  const ProfileStackScreen = ({navigation}) => 
    (
  
    <ProfileStack.Navigator headerMode="none" screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props) => 
                        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
                            <Avatar.Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                }}
                                size={30}
                                style={{marginLeft: 15}}
                                
                                >

                                </Avatar.Image>
                        </TouchableOpacity>),
  }} >
      <ProfileStack.Screen name="Profile" component={ProfileContainer} />
      
  </ProfileStack.Navigator>
    );
  
  
  
  
  
  const LocationMapStackScreen = ({navigation}) => 
  (
  
    <LocationMapStack.Navigator screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props) => 
                        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
                            <Avatar.Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                }}
                                size={30}
                                style={{marginLeft: 15}}
                                
                                >

                                </Avatar.Image>
                        </TouchableOpacity>),
  }} >
      <LocationMapStack.Screen name="Location" component={LocationMapContainer} />
      
  </LocationMapStack.Navigator>
  );
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  const SearchStackScreen = ({navigation}) => 
    (
  
    <SearchStack.Navigator headerMode="none" screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
        
          headerTitleAlign: 'center',
          headerLeft: ((props) => 
                        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
                            <Avatar.Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                }}
                                size={30}
                                style={{marginLeft: 15}}
                                
                                >

                                </Avatar.Image>
                        </TouchableOpacity>),
                        
  }} >
      <SearchStack.Screen name=" "  component={SearchContainer} />
      
  </SearchStack.Navigator>
    );
  
  
  
  
  
  const NotificationsStackScreen = ({navigation}) => 
    (
  
    <NotificationsStack.Navigator screenOptions={{
      headerStyle: {
              backgroundColor: "#fff",
              
          },
          headerTintColor: '#DDA0DD',
          headerTitleStyle: {
              fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerLeft: ((props) => 
                        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
                            <Avatar.Image source={{
                                    uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSPMPR8kZTIDExkEf15iSImqhGt3R-I6Tp_gwOcQKHkRtIQVI1G&usqp=CAU'
                                }}
                                size={30}
                                style={{marginLeft: 15}}
                                
                                >

                                </Avatar.Image>
                        </TouchableOpacity>),
  }} >
      <NotificationsStack.Screen name="Notification" component={NotificationsContainer} />
      
  </NotificationsStack.Navigator>
    );








    
  


    export default MainTabScreen;