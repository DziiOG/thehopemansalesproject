import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Orders from '../components/Orders';
import {
 
} from '../modules/orders';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Orders);
