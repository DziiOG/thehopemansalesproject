import React, { Component,  } from 'react'
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native'


import {Card, CardItem, Thumbnail, Body, Left, Right, Button, Icon} from 'native-base'

const width = Dimensions.get('window').width


export default class CardComponent extends Component {
    
    
    render() {
        const images = {
            "1": require('../../../../assets/fashion.png'),
            "2": require('../../../../assets/fashion1.png'),
            "3": require('../../../../assets/fashion2.png'),
        }
        return (
           <Card>
            <CardItem>
               <Left>
                   <Thumbnail source={require('../../../../assets/me.png')}>
                   </Thumbnail>
                   <Body>
                       <Text>SpongeBob</Text>
                       <Text note>May 2, 2020</Text> 
                   </Body>
               </Left>
            </CardItem>
            <CardItem cardBody>
                 <Image source={images[this.props.imageSource]} style={{
                     height: 200, width: width, flex: 1, overflow: 'hidden'
                 }}></Image>
            </CardItem>
            <CardItem style={{height:45}}>
                <Left>
                     <Button transparent>
                         <Icon 
                         name="ios-heart"
                         style={{color: 'black'}}
                         ></Icon>
                     </Button>
                     <Button transparent>
                         <Icon 
                         name="ios-chatbubbles"
                         style={{color: 'black'}}
                         ></Icon>
                     </Button>
                     <Button transparent>
                         <Icon 
                         name="ios-send"
                         style={{color: 'black'}}
                         ></Icon>
                     </Button> 
                </Left>
            </CardItem>
            <CardItem style={{height: 20}}>
                <Text>{this.props.likes} likes</Text>
            </CardItem>
            <CardItem>
                <Body>
                    <Text>
                    <Text style={{fontWeight: "bold"}}>spongebob </Text>
                        Hahahahahahhah
                    </Text>
                </Body>
            </CardItem>

           </Card>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})