import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Home from '../components/Home';
import {
 
} from '../modules/home';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Home);
