import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import UserProfile from '../components/UserProfile';
import {
 
} from '../modules/userprofile';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(UserProfile);
