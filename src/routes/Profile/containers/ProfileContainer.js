import 'react-native-gesture-handler';

import {connect} from 'react-redux';
import Profile from '../components/Profile';
import {
 
} from '../modules/profile';

const mapStateToProps = state => ({
 
});

const mapActionsCreators = {
  
};

export default connect(mapStateToProps, mapActionsCreators)(Profile);
