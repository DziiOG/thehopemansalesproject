import React, { Component, Fragment } from 'react'
import {  Text, Image, Dimensions } from 'react-native'
import {Container, Content,  View, Button, Icon} from 'native-base'
import CardComponent from '../../Home/components/CardComponent'

var {width, height} = Dimensions.get('window')

var images = [
    require('../../../assets/1.jpg'),
    require('../../../assets/IMG_0475.png'),
    require('../../../assets/IMG_0282.png'),
    require('../../../assets/IMG_0313.png'),
    require('../../../assets/IMG_0390.png'),
    require('../../../assets/IMG_0446.png'),
    require('../../../assets/IMG_0447.png'),
    require('../../../assets/IMG_0448.png'),
    require('../../../assets/IMG_0470.png'),
    require('../../../assets/IMG_0471.png'),

]

var images2 = [
    require('../../../assets/me.png'),
    require('../../../assets/fashion.png'),
    require('../../../assets/fashion1.png'),
]

export default class Profile extends Component {


    constructor(props){
        super(props)
        this.state = {
            activeIndex: 0
        }
    }



    segmentClicked = (index) => {
        this.setState({
            activeIndex: index
        })
    }


    renderSectionOne = () => {
        return images.map((image, index)=> {
            return (
                <View key={index} style={[{width:(width)/3, height: (width)/3},{marginBottom: 2},
                index % 3 !== 0 ? {paddingLeft:2 } : { paddingLeft: 0}
                
                ]}>
                    <Image style={{flex:1, width:undefined, height:undefined}} source={image}></Image>
                </View>
            )
        })
    }

    renderSectionTwo = () => {
        return images2.map((image, index)=> {
            return (
                <View key={index} style={[{width:(width)/3, height: (width)/3},{marginBottom: 2},
                index % 3 !== 0 ? {paddingLeft:2 } : { paddingLeft: 0}
                
                ]}>
                    <Image style={{flex:1, width:undefined, height:undefined}} source={image}></Image>
                </View>
            )
        })
    }


    renderSection = () => {
        if(this.state.activeIndex == 0){
            return(
                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                    {this.renderSectionOne()}
                </View>
            ) 
        }else if( this.state.activeIndex == 1){

            return(
            <Fragment>
                <CardComponent imageSource="1" likes="0"></CardComponent>
                <CardComponent imageSource="2" likes="0"></CardComponent>
                <CardComponent imageSource="3" likes="0"></CardComponent>
            </Fragment>
            )
        }else if(this.state.activeIndex == 3){
            
                return (
                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                   {this.renderSectionTwo()}
                </View>
                )
            
        }
    }





    render() {
        return (
          <Container style={{backgroundColor: 'white', flex: 1}}>
                <Content>
                   <View style={{paddingTop: 10}}>
                       <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1, alignContent: 'center', marginLeft: 5}}>
                               <Image source={require('../../../assets/me.png')}
                               style={{ width: 75, height: 75, borderRadius: 37.5}}
                               ></Image>
                            </View>
                            <View style={{flex: 3}}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                                    <View style={{alignItems: 'center'}}>
                                        <Text>0</Text>
                                        <Text style={{fontSize: 10, color: 'grey'}}>Designs</Text>
                                    </View>
                                    <View style={{alignItems: 'center'}}>
                                        <Text>0</Text>
                                        <Text style={{fontSize: 10, color: 'grey'}}>followers</Text>
                                    </View>
                                    <View style={{alignItems: 'center'}}>
                                        <Text>0</Text>
                                        <Text style={{fontSize: 10, color: 'grey'}}>Following</Text>
                                    </View>
                                </View>
                                <View style={{flexDirection: 'row', paddingTop: 10}}>
                                        <Button bordered dark
                                            style={{ flex: 3, marginLeft: 10, justifyContent: 'center', height: 30, borderBottomEndRadius: 20,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    borderTopEndRadius: 20,
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center'}}
                                        >
                                            <Text>Edit Profile</Text>
                                        </Button>

                                        <Button bordered dark style={{flex: 1, height:30, marginRight: 10, marginLeft: 5, justifyContent: 'center', borderBottomEndRadius: 20,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    borderTopEndRadius: 20,
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center'}}>
                                                <Icon name="settings"></Icon>
                                        </Button>
                                </View>
                            </View>
                       </View>
                       <View style={{paddingVertical: 10, paddingHorizontal: 10}}>
                           <Text style={{ fontWeight: 'bold'}}>SpongeBob</Text>
                           <Text></Text>
                           <Text></Text>
                       </View>
                   </View>
                   <View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around', marginBottom: 5}}>
                                <Button 
                                onPress={()=> this.segmentClicked(0)}
                                active={this.state.activeIndex == 0}
                                style={{
                                    borderBottomEndRadius: 20,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    borderTopEndRadius: 20,
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 73, 
                                    height: 37, 
                                    backgroundColor:this.state.activeIndex == 0 ?   '#dda0dd' : '#fff' ,
                                    }}
                                >
                                        <Icon name="ios-apps"
                                        style={[this.state.activeIndex == 0 ? {}: {color: 'grey'}]}
                                        ></Icon>
                                </Button>
                                <Button 
                                 onPress={()=> this.segmentClicked(1)}
                                active={this.state.activeIndex == 1}
                                style={{
                                    borderBottomEndRadius: 20,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    borderTopEndRadius: 20,
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 73, 
                                    height: 37, 
                                    backgroundColor:this.state.activeIndex == 1 ?  '#dda0dd' : '#fff' ,
                                    }}
                                >
                                        <Icon name="ios-list"
                                        style={[this.state.activeIndex == 1 ? {}: {color: 'grey'}]}
                                        ></Icon>
                                </Button>
                                
                                <Button 
                                 onPress={()=> this.segmentClicked(3)}
                                active={this.state.activeIndex == 3}
                                style={{
                                    borderBottomEndRadius: 20,
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    borderTopEndRadius: 20,
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 73, 
                                    height: 37, 
                                    backgroundColor:this.state.activeIndex == 3 ?  '#dda0dd' : '#fff' ,
                                    }}
                                >
                                        <Icon name="ios-bookmark"
                                        style={[this.state.activeIndex == 3 ? {}: {color: 'grey'}]}
                                        ></Icon>
                                </Button>
                        </View>

                        {this.renderSection()}

                   </View>
                </Content>
          </Container>
        )
    }
}
