import { combineReducers } from 'redux';

import { homeReducer as home } from "../routes/Home/modules/home";
import { loginReducer as login } from "../routes/Login/modules/login";
import { measurementsReducer as measurements } from "../routes/Measurements/modules/measurements";
import { ordersReducer as orders } from "../routes/Orders/modules/orders";
import { settingsReducer as settings } from "../routes/Settings/modules/settings";
import { signupReducer as signup } from "../routes/SignUp/modules/signup";

import { searchReducer as search } from "../routes/Search/modules/search";
import { locationmapReducer as locationmap } from "../routes/LocationMap/modules/locationmap";
import { profileReducer as profile } from "../routes/Profile/modules/profile";
import { userprofileReducer as userprofile } from "../routes/Profile/modules/userprofile";
import { notificationsReducer as notifications } from "../routes/Notifications/modules/notifications";
import { designsReducer as designs } from "../routes/Designs/modules/designs";
import { messagesReducer as messages } from "../routes/Messages/modules/messages";




export const makeRootReducer = () => {
    return combineReducers({
        home,
        login,
        measurements,
        orders,
        settings,
        signup,
        search,
        profile,
        userprofile,
        locationmap,
        notifications,
        designs,
        messages
    })
}

export default makeRootReducer;